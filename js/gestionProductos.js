var productosObtenidos;

function getProducts() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products/";
  var request = new XMLHttpRequest();
  // Configuracion basada en eventos
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) { // Estados: 4: rta
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  };

  request.open("GET",url,true);
  request.send();
};

function procesarProductos(){
  var jSONProductos = JSON.parse(productosObtenidos);
  //alert(jSONProductos.value[0].ProductName);
  var divTabla = document.getElementById('divTablaProductos');
  var tabla = document.createElement("table");
  var tBody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");




  for (var i = 0; i < jSONProductos.value.length; i++) {
    //console.log( i + ":" + jSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnName = document.createElement("td");
    columnName.innerText = jSONProductos.value[i].ProductName;

    var columnPrice = document.createElement("td");
    columnPrice.innerText = jSONProductos.value[i].UnitPrice;

    var columnUnits = document.createElement("td");
    columnUnits.innerText = jSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnName);
    nuevaFila.appendChild(columnPrice);
    nuevaFila.appendChild(columnUnits);
    tBody.appendChild(nuevaFila);


  }
  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);



};
