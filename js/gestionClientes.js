var clientesObtenidos;

function getCustomers() {
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers/?$filter=Country%20eq%20'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers/";
  var request = new XMLHttpRequest();
  // Configuracion basada en eventos
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) { // Estados: 4: rta
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  };

  request.open("GET",url,true);
  request.send();

};

function procesarClientes(){
  var jSONClientes = JSON.parse(clientesObtenidos);
  var flagURL = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(jSONProductos.value[0].ProductName);
  var divTabla = document.getElementById('divTablaClientes');
  var tabla = document.createElement("table");
  var tBody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < jSONClientes.value.length; i++) {
    //console.log( i + ":" + jSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnName = document.createElement("td");
    columnName.innerText = jSONClientes.value[i].ContactName;

    var columnCity = document.createElement("td");
    columnCity.innerText = jSONClientes.value[i].City;

    var columnFlag = document.createElement("td");
    var imgFlag = document.createElement("img");
    imgFlag.classList.add("flag");
    //columnFlag.innerText = jSONClientes.value[i].UnitsInStock;
    imgFlag.src = flagURL + (jSONClientes.value[i].Country == 'UK' ? "United-Kingdom" : jSONClientes.value[i].Country ) + ".png";
    //imgFlag.src = flagURL + jSONClientes.value[i].Country + ".png";
    columnFlag.appendChild(imgFlag);

    nuevaFila.appendChild(columnName);
    nuevaFila.appendChild(columnCity);
    nuevaFila.appendChild(columnFlag);
    tBody.appendChild(nuevaFila);


  }
  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);



};
